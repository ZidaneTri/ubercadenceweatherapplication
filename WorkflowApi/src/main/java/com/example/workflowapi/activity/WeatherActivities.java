package com.example.workflowapi.activity;

import com.uber.cadence.activity.ActivityMethod;

public interface WeatherActivities{

    @ActivityMethod(scheduleToCloseTimeoutSeconds = 40)
    String retrieveWeatherData(String city);

    @ActivityMethod(scheduleToCloseTimeoutSeconds = 40)
    void storeWeatherData(String city, String temp);


}

package com.example.workflowworker;

import com.example.workflowworker.workflow.WeatherWorkflowImpl;
import com.uber.cadence.client.WorkflowClient;
import com.uber.cadence.client.WorkflowClientOptions;
import com.uber.cadence.serviceclient.ClientOptions;
import com.uber.cadence.serviceclient.WorkflowServiceTChannel;
import com.uber.cadence.worker.Worker;
import com.uber.cadence.worker.WorkerFactory;

import static com.example.workflowapi.workflow.WeatherWorkflow.TASK_LIST;
import static com.example.workflowapi.workflow.WeatherWorkflow.WEATHER_DOMAIN;

public class WorkflowWorkerApplication {


    public static void main(String[] args) {

        WorkflowClient workflowClient =
                WorkflowClient.newInstance(
                        new WorkflowServiceTChannel(ClientOptions.defaultInstance()),
                        WorkflowClientOptions.newBuilder().setDomain(WEATHER_DOMAIN).build());

        WorkerFactory factory = WorkerFactory.newInstance(workflowClient);
        Worker worker = factory.newWorker(TASK_LIST);
        worker.registerWorkflowImplementationTypes(WeatherWorkflowImpl.class);
        factory.start();
    }


}

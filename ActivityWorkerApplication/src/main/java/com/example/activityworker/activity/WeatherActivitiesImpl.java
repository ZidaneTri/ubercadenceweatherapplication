package com.example.activityworker.activity;

import com.example.activityworker.entity.TemperatureData;
import com.example.activityworker.repository.TemperatureDataRepository;
import com.example.activityworker.util.TemperatureDeserializer;
import com.example.workflowapi.activity.WeatherActivities;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import org.springframework.stereotype.Component;
import javax.transaction.Transactional;
import java.lang.reflect.Type;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.LocalDateTime;

@Component
public class WeatherActivitiesImpl implements WeatherActivities {

    private static final String API_KEY = "&appid=2ceccf4a26c5789e34690ee2b67b3def";

    private static final String WEATHER_URI = "https://api.openweathermap.org/data/2.5/weather?units=metric&q=";

    final
    TemperatureDataRepository temperatureDataRepository;

    public WeatherActivitiesImpl(TemperatureDataRepository temperatureDataRepository) {
        this.temperatureDataRepository = temperatureDataRepository;
    }

    @Override
    public String retrieveWeatherData(String city) {

        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(WEATHER_URI + city + API_KEY))
                .build();
        String result =  client.sendAsync(request, HttpResponse.BodyHandlers.ofString())
                .thenApply(HttpResponse::body)
                .join();

        Type type = new TypeToken<String>(){}.getType();
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(type, new TemperatureDeserializer())
                .create();

        return gson.fromJson(result, type);
    }
    @Override
    @Transactional
    public void storeWeatherData(String city, String temp) {
        TemperatureData currentData = new TemperatureData(city, temp, LocalDateTime.now().toString());
        temperatureDataRepository.save(currentData);
    }

}

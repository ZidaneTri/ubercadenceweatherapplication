package com.example.activityworker.config;


import com.uber.cadence.client.WorkflowClient;
import com.uber.cadence.client.WorkflowClientOptions;
import com.uber.cadence.serviceclient.ClientOptions;
import com.uber.cadence.serviceclient.IWorkflowService;
import com.uber.cadence.serviceclient.WorkflowServiceTChannel;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static com.example.workflowapi.workflow.WeatherWorkflow.WEATHER_DOMAIN;

@Configuration
public class WorkerConfig {

    @Bean
    public WorkflowClientOptions workflowClientOptions() {
        return WorkflowClientOptions.newBuilder()
                .setDomain(WEATHER_DOMAIN)
                .build();
    }

    @Bean
    @ConditionalOnMissingBean(WorkflowClient.class)
    public WorkflowClient workflowClient(IWorkflowService workflowService, WorkflowClientOptions workflowClientOptions) {
        return WorkflowClient.newInstance(workflowService, workflowClientOptions);
    }


    @Bean
    @ConditionalOnMissingBean(IWorkflowService.class)
    public IWorkflowService cadenceClient() {
        return new WorkflowServiceTChannel(ClientOptions.defaultInstance());
    }
}

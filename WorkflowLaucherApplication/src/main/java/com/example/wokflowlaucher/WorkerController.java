package com.example.wokflowlaucher;

import com.example.workflowapi.workflow.WeatherWorkflow;
import com.uber.cadence.*;
import com.uber.cadence.client.WorkflowClient;
import com.uber.cadence.client.WorkflowOptions;
import org.apache.thrift.TException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import static com.example.workflowapi.workflow.WeatherWorkflow.TASK_LIST;
import static com.example.workflowapi.workflow.WeatherWorkflow.WEATHER_DOMAIN;

@RestController
public class WorkerController {

    final
    WorkflowClient workflowClient;

    private final String WORKFLOW_ID = "weather-workflow-id";

    public WorkerController(WorkflowClient workflowClient) {
        this.workflowClient = workflowClient;
    }

    @GetMapping("/getweather/{city}")
    public ResponseEntity<String> startWorkflow(@PathVariable String city) {

        try {
            StartWorkflowExecutionResponse responseBody =  workflowClient.getService()
                    .StartWorkflowExecution(new StartWorkflowExecutionRequest()
                            .setTaskList(new TaskList().setName(TASK_LIST))
                            .setDomain(WEATHER_DOMAIN)
                            .setWorkflowId(WORKFLOW_ID)
                            .setWorkflowType(new WorkflowType().setName("WeatherWorkflow::getWeather"))
                            .setTaskStartToCloseTimeoutSeconds(10)
                            .setExecutionStartToCloseTimeoutSeconds(60)
                            .setWorkflowIdReusePolicy(WorkflowIdReusePolicy.AllowDuplicate)
                            .setInput(city.getBytes()));

            if(responseBody.isSetRunId() && executionResult(new WorkflowExecution().setRunId(responseBody.getRunId()).setWorkflowId(WORKFLOW_ID))){
                return ResponseEntity.ok("All Done!");
            } else return ResponseEntity.badRequest().body("Fail");

        } catch (TException e){
            return ResponseEntity.badRequest().body("Got exception");
        }
    }

    @GetMapping("option/getweather/{city}")
    public ResponseEntity<String> startWorkflowOptional(@PathVariable String city) {

        try {
            WorkflowOptions workflowOptions =
                    new WorkflowOptions.Builder()
                            .setTaskList(TASK_LIST)
                            .setWorkflowId(WORKFLOW_ID)
                            .setWorkflowIdReusePolicy(WorkflowIdReusePolicy.AllowDuplicate)
                            .build();
            WeatherWorkflow weatherWorkflow =
                    workflowClient.newWorkflowStub(WeatherWorkflow.class, workflowOptions);

            WorkflowExecution execution = WorkflowClient.start(weatherWorkflow::getWeather,city);

            if(executionResult(execution)){
                return ResponseEntity.ok("All Done!");
            } else return ResponseEntity.badRequest().body("Fail");

        } catch (TException e){
            return ResponseEntity.badRequest().body("Got exception");
        }
    }

    private boolean executionResult(WorkflowExecution execution) throws TException {

        DescribeWorkflowExecutionResponse describeResponseBody = workflowClient.getService()
                .DescribeWorkflowExecution(new DescribeWorkflowExecutionRequest()
                        .setDomain(WEATHER_DOMAIN)
                        .setExecution(execution));
        return !describeResponseBody.isSetPendingActivities();
    }


}